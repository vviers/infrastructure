resource "kubernetes_cron_job" "matomo_backup" {
  count = var.backup_bucket == null ? 0 : 1
  metadata {
    name      = "matomo-backup"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "25 3 * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {}
            labels      = {}
          }
          spec {
            container {
              name  = "matomo-backup"
              image = "registry.gitlab.com/vigigloo/tools/matomo/backup:897afe1566d665901b4942c3a3908de22419d959"

              resources {
                limits = {
                  cpu    = "500m"
                  memory = "512Mi"
                }
              }

              env {
                name  = "S3_BUCKET_NAME"
                value = scaleway_object_bucket.matomo_backups[0].name
              }

              env {
                name  = "S3_REGION"
                value = scaleway_object_bucket.matomo_backups[0].region
              }

              env {
                name  = "S3_ENDPOINT_URL"
                value = scaleway_object_bucket.matomo_backups[0].endpoint
              }

              env {
                name  = "S3_ACCESS_KEY"
                value = var.scaleway_access_key
              }

              env {
                name  = "S3_SECRET_KEY"
                value = var.scaleway_secret_key
              }

              env {
                name  = "MYSQL_HOST"
                value = module.mariadb.host
              }

              env {
                name  = "MYSQL_USER"
                value = "root"
              }

              env {
                name  = "MYSQL_PASSWORD"
                value = module.mariadb.root_password
              }

              volume_mount {
                name       = "data"
                mount_path = "/data"
                read_only  = true
              }
            }

            affinity {
              pod_affinity {
                required_during_scheduling_ignored_during_execution {
                  label_selector {
                    match_expressions {
                      key      = "app.kubernetes.io/name"
                      operator = "In"
                      values   = ["matomo"]
                    }
                  }
                  topology_key = "kubernetes.io/hostname"
                }
              }
            }

            volume {
              name = "data"
              persistent_volume_claim {
                claim_name = "matomo"
              }
            }
          }
        }
      }
    }
  }
}
