resource "random_password" "matomo_db_password" {
  length  = 30
  special = false
  lifecycle {
    ignore_changes = all
  }
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = 2
  max_memory_limits = "3Gi"
  namespace         = "tools-matomo"
  project_name      = "Matomo"
  project_slug      = "matomo"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

module "mariadb" {
  source        = "gitlab.com/vigigloo/tools-k8s/mariadb"
  version       = "0.1.1"
  chart_name    = "mariadb"
  namespace     = module.namespace.namespace
  chart_version = "11.4.3"

  mariadb_auth_database = "matomo"
  mariadb_auth_username = "matomo"
  mariadb_auth_password = random_password.matomo_db_password.result

  limits_cpu      = null
  requests_memory = null
  limits_memory   = "512Mi"
}

module "matomo" {
  source        = "gitlab.com/vigigloo/tools-k8s/matomo"
  version       = "1.0.0"
  chart_name    = "matomo"
  chart_version = "1.5.2"
  namespace     = module.namespace.namespace
  image_tag     = "4.13.1-fpm-alpine"
  values = [
    <<-EOT
  ingress:
    annotations:
      kubernetes.io/ingress.class: haproxy
      acme.cert-manager.io/http01-edit-in-place: "true"
      cert-manager.io/cluster-issuer: letsencrypt-prod
      haproxy.org/response-set-header: Referrer-Policy
    hosts:
      - ${var.hostname}
    tls:
      - secretName: matomo-tls
        hosts:
          - ${var.hostname}
    EOT
  ]

  limits_cpu      = "1"
  limits_memory   = "1024Mi"
  requests_cpu    = "1"
  requests_memory = "1024Mi"

  matomo_database_host     = module.mariadb.host
  matomo_database_dbname   = "matomo"
  matomo_database_username = "matomo"
  matomo_database_password = random_password.matomo_db_password.result
}
