locals {
  backups_enabled   = var.backup_bucket_db != null
  backup_offset_min = 40
}
resource "scaleway_object_bucket" "db_backups" {
  count = local.backups_enabled ? 1 : 0
  name  = var.backup_bucket_db
}

module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = "9"
  max_memory_limits = "20Gi"
  namespace         = "tools-mobilizon"
  project_slug      = "mobilizon"
  project_name      = "Mobilizon"

  default_container_cpu_requests  = "10m"
  default_container_memory_limits = "16Mi"
}

module "postgresql" {
  source         = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version        = "0.0.22"
  namespace      = module.namespace.namespace
  chart_name     = "postgresql"
  pg_replicas    = 3
  pg_volume_size = "5Gi"

  pg_backups_volume_enabled             = true
  pg_backups_volume_size                = "20Gi"
  pg_backups_volume_full_schedule       = "${local.backup_offset_min} 2 * * 0"
  pg_backups_volume_incr_schedule       = "${local.backup_offset_min} 2 * * 1-6"
  pg_backups_volume_full_retention      = 4
  pg_backups_volume_full_retention_type = "count"

  pg_backups_s3_enabled       = local.backups_enabled
  pg_backups_s3_bucket        = local.backups_enabled ? resource.scaleway_object_bucket.db_backups[0].name : null
  pg_backups_s3_region        = local.backups_enabled ? resource.scaleway_object_bucket.db_backups[0].region : null
  pg_backups_s3_endpoint      = local.backups_enabled ? "s3.${resource.scaleway_object_bucket.db_backups[0].region}.scw.cloud" : null
  pg_backups_s3_access_key    = local.backups_enabled ? var.scaleway_access_key : null
  pg_backups_s3_secret_key    = local.backups_enabled ? var.scaleway_secret_key : null
  pg_backups_s3_full_schedule = local.backups_enabled ? "${local.backup_offset_min} 3 * * 0" : null
  pg_backups_s3_incr_schedule = local.backups_enabled ? "${local.backup_offset_min} 3 * * 1-6" : null

  values = [
    file("${path.module}/postgresql.yaml")
  ]
}

resource "random_password" "secret_key" {
  length = 69 # equivalent to 50 bytes encoded in b64 like in mobilizon's docs
}
resource "random_password" "secret_key_base" {
  length = 69
}
resource "kubernetes_secret" "mobilizon_keys" {
  metadata {
    namespace = module.namespace.namespace
    name      = "mobilizon-keys"
  }
  data = {
    "secretKey"     = random_password.secret_key.result
    "secretKeyBase" = random_password.secret_key_base.result
  }
}

resource "helm_release" "mobilizon" {
  name       = "mobilizon"
  namespace  = module.namespace.namespace
  chart      = "mobilizon"
  repository = "https://gitlab.com/api/v4/projects/45131100/packages/helm/stable"
  version    = "0.2.1"
  values = [<<-EOT
    envVars:
      MOBILIZON_INSTANCE_NAME: ${var.instance_name}
      MOBILIZON_INSTANCE_DESCRIPTION: ${var.instance_description}
      MOBILIZON_INSTANCE_HOST: ${var.hostname}
      MOBILIZON_INSTANCE_EMAIL: ${var.smtp_from_email}
      MOBILIZON_REPLY_EMAIL: ${var.smtp_reply_email}
      MOBILIZON_INSTANCE_REGISTRATIONS_OPEN: true
      MOBILIZON_SMTP_SERVER: ${var.smtp_host}
      MOBILIZON_SMTP_PORT: ${var.smtp_port}
      MOBILIZON_SMTP_USERNAME: ${var.smtp_user}
      MOBILIZON_SMTP_PASSWORD: ${var.smtp_password}
      MOBILIZON_DATABASE_USERNAME:
        secretKeyRef:
          name: postgresql-pguser-postgresql
          key: user
      MOBILIZON_DATABASE_PASSWORD:
        secretKeyRef:
          name: postgresql-pguser-postgresql
          key: password
      MOBILIZON_DATABASE_DBNAME:
        secretKeyRef:
          name: postgresql-pguser-postgresql
          key: dbname
      MOBILIZON_DATABASE_HOST:
        secretKeyRef:
          name: postgresql-pguser-postgresql
          key: host
      MOBILIZON_DATABASE_SSL: true
      MOBILIZON_INSTANCE_SECRET_KEY_BASE:
        secretKeyRef:
          name: mobilizon-keys
          key: secretKeyBase
      MOBILIZON_INSTANCE_SECRET_KEY:
        secretKeyRef:
          name: mobilizon-keys
          key: secretKey
    resources:
      limits:
        memory: 1Gi
      requests:
        cpu: 100m
    ingress:
      enabled: true
      host: ${var.hostname}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
    EOT
  ]

  depends_on = [
    module.postgresql
  ]
}
