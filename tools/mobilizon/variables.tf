variable "hostname" {
  type = string
}

variable "instance_name" {
  type = string
}
variable "instance_description" {
  type = string
}

variable "backup_bucket_db" {
  type    = string
  default = null
}
variable "backup_bucket_uploads" {
  type    = string
  default = null
}

variable "scaleway_access_key" {
  type      = string
  sensitive = true
  default   = null
}

variable "scaleway_secret_key" {
  type      = string
  sensitive = true
  default   = null
}

variable "smtp_host" {
  type = string
}
variable "smtp_port" {
  type = number
}
variable "smtp_user" {
  type = string
}
variable "smtp_password" {
  type      = string
  sensitive = true
}
variable "smtp_from_email" {
  type = string
}
variable "smtp_reply_email" {
  type = string
}
