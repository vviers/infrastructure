module "cluster_production" {
  providers = {
    scaleway.iam             = scaleway.iam
    scaleway.default_project = scaleway.default_project
    scaleway                 = scaleway.cluster_production
  }
  source              = "./clusters/production"
  dns_zone_incubateur = local.dns_zone_incubateur
  scaleway_project_id = var.scaleway_cluster_production_project_id
  scaleway_access_key = var.scaleway_cluster_production_access_key
  scaleway_secret_key = var.scaleway_cluster_production_secret_key

  scaleway_default_project_id = var.scaleway_default_project_id
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key

  mattermost_postgresql = var.production_tools_mattermost_postgresql

  outline_postgresql          = var.production_tools_outline_postgresql
  outline_client_id           = var.production_tools_outline_client_id
  outline_client_secret       = var.production_tools_outline_client_secret
  outline_email_smtp_host     = var.sendinblue_smtp_host
  outline_email_smtp_user     = var.production_tools_outline_email_smtp_user
  outline_email_smtp_password = var.production_tools_outline_email_smtp_password

  decidim_inclusion_numerique_smtp_user     = var.production_tools_decidim_inclusion_numerique_smtp_user
  decidim_inclusion_numerique_smtp_password = var.production_tools_decidim_inclusion_numerique_smtp_password

  sendinblue_smtp_host     = var.sendinblue_smtp_host
  sendinblue_smtp_user     = var.sendinblue_smtp_user
  sendinblue_smtp_password = var.sendinblue_smtp_password
  sendinblue_smtp_port     = var.sendinblue_smtp_port
  sendinblue_smtp_secure   = var.sendinblue_smtp_secure

  grist_oauth_client_id     = var.production_tools_grist_oauth_client_id
  grist_oauth_client_secret = var.production_tools_grist_oauth_client_secret
  grist_default_email       = var.production_tools_grist_default_email
  grist_oauth_domain        = var.production_tools_grist_oauth_domain
  grist_monitoring_org_id   = random_string.org_id_shared_donnees_territoires_transverse.result

  passbolt_smtp_password = var.production_tools_passbolt_smtp_password
}

module "cluster_development" {
  providers = {
    scaleway.iam             = scaleway.iam
    scaleway.default_project = scaleway.default_project
    scaleway                 = scaleway.cluster_development
  }
  source              = "./clusters/development"
  dns_zone_incubateur = local.dns_zone_incubateur
  scaleway_project_id = var.scaleway_cluster_development_project_id
  scaleway_access_key = var.scaleway_cluster_development_access_key
  scaleway_secret_key = var.scaleway_cluster_development_secret_key

  scaleway_default_project_id = var.scaleway_default_project_id
  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key

  sendinblue_smtp_host     = var.sendinblue_smtp_host
  sendinblue_smtp_user     = var.sendinblue_smtp_user
  sendinblue_smtp_password = var.sendinblue_smtp_password
  sendinblue_smtp_port     = var.sendinblue_smtp_port

  grist_oauth_client_id     = var.development_tools_grist_oauth_client_id
  grist_oauth_client_secret = var.development_tools_grist_oauth_client_secret
  grist_default_email       = var.development_tools_grist_default_email
  grist_oauth_domain        = var.development_tools_grist_oauth_domain
  grist_monitoring_org_id   = random_string.org_id_shared_donnees_territoires_transverse.result
}

moved {
  from = module.cluster-production
  to   = module.cluster_production
}
moved {
  from = module.cluster-development
  to   = module.cluster_development
}
