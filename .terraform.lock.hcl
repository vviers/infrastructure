# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "16.0.3"
  constraints = ">= 15.7.0, ~> 16.0"
  hashes = [
    "h1:NBCRbq9yjFnIifKqWzcfyYsUNH454Mgn4IrIIK7VJbA=",
    "h1:RDuwB4If/4RvmNBa77+FvC+E0kfft7tih1QNSbw2QXI=",
    "zh:2e94e62242b92bd50493934523f522ba48fa2454fe8c8044e8bebd1c243ee655",
    "zh:3f47e1e92da6be38f6ddcfe23d24c41967c8374df4bd925ae45252b915f35a98",
    "zh:4e69de8d2bff4afde7f84f6be9fbc72f6b26831ea9aaba840618dba879ffd4fc",
    "zh:5c891d3c164cd45344faddefa0cc37211de5348f07fa766bd53cbb85cc6f55e9",
    "zh:747b3603bae67345990b49ed40c72e18e68e5459bb9a6d56f49c142e036503d0",
    "zh:872324d8c7471ca8f7b9da67a5016c5d22403f275cbba2c101e199d74d242196",
    "zh:8afee76e0dd0c3a17301c8d9482390b4daef57a35d2ab484dfb57bdb77d9b66b",
    "zh:8b55c6509d7fabeabba33e392a65302dbdc0583e4d0b32278a6f21efbc3cabbb",
    "zh:b0c48767a25a78c451587a7dd06171e58eb5a4086ecfd5c2d797a3c20989b6e8",
    "zh:c4a982d6e5198442a2f5d08c2c9c6273c78bc4fd2eda094b8ad3199c42308843",
    "zh:c71fdf664e564e5b7c0b6fe3bd93684befe162e0de2cb5a5e85bf92f59519be6",
    "zh:dc566e88a74cc5ae2cdaabb871b852a200c40edf77985e3a32d83efc4b2dfd21",
    "zh:f5277be064e7417bcbfe238459620648ea055ebae1d024957e3e92a37e772ea6",
    "zh:f809ab383cca0a5f83072981c64208cbd7fa67e986a86ee02dd2c82333221e32",
    "zh:fd7c977b6711e7a242b3454f9afe1ab32c402d3440a460ea35a4fa7c0c8b6e9c",
  ]
}

provider "registry.terraform.io/grafana/grafana" {
  version     = "1.39.0"
  constraints = "~> 1.39.0"
  hashes = [
    "h1:1mM6Lo7QLyOsRYp3dPAFPmBcFHqyJO/YK6l92zjQXNg=",
    "h1:XnopxRTFHnNQ7CXBKgEu8+/0pUyeyAF4Vnw2SNF2UYM=",
    "zh:44a628995f19af4aaffd1850636d53629b378bb024294315412e5dd38b44346b",
    "zh:4728b1852e8373e5bfdecb08516eca6a23d1a839aeec70272f4aacce87371987",
    "zh:58990894ceeebb389202b15d1d4566b1ef50b2f1ad5b8a74843f6cce1fa3d8b1",
    "zh:60d5258498f6f3d223c3d2fbdfd80c48f7f94f848afd2d020435d3adfe40181d",
    "zh:6511cfcac3e94307af9a934fa947ea7f35c807ad27fff4e4c69f3355d22ded38",
    "zh:7b68d76e3e423aaedb4a76b73a2cc5f92d3647512d0291cd9cae1f0bce4b2053",
    "zh:9819f231ff2dbac0e03a292ef3174b5b219e0611e41991afe8fe2b1592d43c24",
    "zh:a582a002c78eece4361433f661db2c351864ef9b73ac8208fbad39897d93de95",
    "zh:a8f6bedc2c21e8b300eeffe892105e0fd2d25fef26f58ed573404f94090b3439",
    "zh:b65ee5850289459e8a6d21d2229756405aec897e23794c452c21e00c6ff81c6f",
    "zh:ce5ca97ae36c39a5aa1fdf5714b8204d98d32d33009febd0758e6414a032727b",
    "zh:da1e8b938b7f93990a3e960b4bcb4768c8f6187161646dfcbd47b636e4725d8c",
    "zh:f22131b245519ee605b82ff13ce686b1f5301b8293532c97f4de8a5a2cdf2153",
    "zh:fe92388019044cdae3e7112bac5896bc0c3523e1065764f0c88308edae185199",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.5.1"
  constraints = "~> 2.0, ~> 2.5.0"
  hashes = [
    "h1:9yMFsXyHAo+mUuMKczNSw44HcZaf1JkMqgOUgJF1dXs=",
    "h1:NasRPC0qqlpGqcF3dsSoOFu7uc5hM+zJm+okd8FgrnQ=",
    "zh:140b9748f0ad193a20d69e59d672f3c4eda8a56cede56a92f931bd3af020e2e9",
    "zh:17ae319466ed6538ad49e011998bb86565fe0e97bc8b9ad7c8dda46a20f90669",
    "zh:3a8bd723c21ba70e19f0395ed7096fc8e08bfc23366f1c3f06a9107eb37c572c",
    "zh:3aae3b82adbe6dca52f1a1c8cf51575446e6b0f01f1b1f3b30de578c9af4a933",
    "zh:3f65221f40148df57d2888e4f31ef3bf430b8c5af41de0db39a2b964e1826d7c",
    "zh:650c74c4f46f5eb01df11d8392bdb7ebee3bba59ac0721000a6ad731ff0e61e2",
    "zh:930fb8ab4cd6634472dfd6aa3123f109ef5b32cbe6ef7b4695fae6751353e83f",
    "zh:ae57cd4b0be4b9ca252bc5d347bc925e35b0ed74d3dcdebf06c11362c1ac3436",
    "zh:d15b1732a8602b6726eac22628b2f72f72d98b75b9c6aabceec9fd696fda696a",
    "zh:d730ede1656bd193e2aea5302acec47c4905fe30b96f550196be4a0ed5f41936",
    "zh:f010d4f9d8cd15936be4df12bf256cb2175ca1dedb728bd3a866c03d2ee7591f",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.13.1"
  constraints = ">= 2.13.0, ~> 2.13, ~> 2.13.1"
  hashes = [
    "h1:1cRcvMGxS9q2Y0PxOrPiLU+nbNERuXML2liAQsWXByU=",
    "h1:cN3OwZvhtn/y3XfnGQ4hi+7oZp1gU2zVYhznRv2C7Qg=",
    "zh:061f6ecbbf9a3c6345b56c28ebc2966a05d8eb02f3ba56beedd66e4ea308e332",
    "zh:2119beeccb35bc5d1392b169f9fc748865261b45fb75fc8f57200e91658837c6",
    "zh:26c29083d0d84fbc2e356e3dd1db3e2dc4139e943acf7a318d3c98f954ac6bd6",
    "zh:2fb5823345ab05b3df74bb5c51c61072637d01b3cddffe3ad36a73b7d5b749e6",
    "zh:3475b4422fffaf58584c4d877f98bfeff075e4a746f13e985d2cb20adc873a6c",
    "zh:366b4bef49932d1d71b12849c1878c254a887962ff915f37982299c1185dd48a",
    "zh:589f9358e4a4bd74a83b97ccc64df455ddfa64c4c4e099aef30fa29080497a8a",
    "zh:7a0d75e0e4fee6cc5599ac9d5e91de563ce9ea7bd8137480c7abd09642a9e72c",
    "zh:a297a42aefe0650e3d9fbe55a3ee48b14bb8bb5edb7068c09512d72afc3d9ca5",
    "zh:b7f83a89b646542d02b733d464e45d6d0739a9dbb921305e7b8347e9fc98a149",
    "zh:d4c721174a598b66bd1b29c40fa7cffafe90bb58186cd7506d792a6b04161103",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.5.1"
  constraints = "~> 3.5.1"
  hashes = [
    "h1:IL9mSatmwov+e0+++YX2V6uel+dV6bn+fC/cnGDK3Ck=",
    "h1:VSnd9ZIPyfKHOObuQCaKfnjIHRtR7qTw19Rz8tJxm+k=",
    "zh:04e3fbd610cb52c1017d282531364b9c53ef72b6bc533acb2a90671957324a64",
    "zh:119197103301ebaf7efb91df8f0b6e0dd31e6ff943d231af35ee1831c599188d",
    "zh:4d2b219d09abf3b1bb4df93d399ed156cadd61f44ad3baf5cf2954df2fba0831",
    "zh:6130bdde527587bbe2dcaa7150363e96dbc5250ea20154176d82bc69df5d4ce3",
    "zh:6cc326cd4000f724d3086ee05587e7710f032f94fc9af35e96a386a1c6f2214f",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b6d88e1d28cf2dfa24e9fdcc3efc77adcdc1c3c3b5c7ce503a423efbdd6de57b",
    "zh:ba74c592622ecbcef9dc2a4d81ed321c4e44cddf7da799faa324da9bf52a22b2",
    "zh:c7c5cde98fe4ef1143bd1b3ec5dc04baf0d4cc3ca2c5c7d40d17c0e9b2076865",
    "zh:dac4bad52c940cd0dfc27893507c1e92393846b024c5a9db159a93c534a3da03",
    "zh:de8febe2a2acd9ac454b844a4106ed295ae9520ef54dc8ed2faf29f12716b602",
    "zh:eab0d0495e7e711cca367f7d4df6e322e6c562fc52151ec931176115b83ed014",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version = "0.9.1"
  hashes = [
    "h1:NUv/YtEytDQncBQ2mTxnUZEy/rmDlPYmE9h2iokR0vk=",
    "h1:VxyoYYOCaJGDmLz4TruZQTSfQhvwEcMxvcKclWdnpbs=",
    "zh:00a1476ecf18c735cc08e27bfa835c33f8ac8fa6fa746b01cd3bcbad8ca84f7f",
    "zh:3007f8fc4a4f8614c43e8ef1d4b0c773a5de1dcac50e701d8abc9fdc8fcb6bf5",
    "zh:5f79d0730fdec8cb148b277de3f00485eff3e9cf1ff47fb715b1c969e5bbd9d4",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:8c8094689a2bed4bb597d24a418bbbf846e15507f08be447d0a5acea67c2265a",
    "zh:a6d9206e95d5681229429b406bc7a9ba4b2d9b67470bda7df88fa161508ace57",
    "zh:aa299ec058f23ebe68976c7581017de50da6204883950de228ed9246f309e7f1",
    "zh:b129f00f45fba1991db0aa954a6ba48d90f64a738629119bfb8e9a844b66e80b",
    "zh:ef6cecf5f50cda971c1b215847938ced4cb4a30a18095509c068643b14030b00",
    "zh:f1f46a4f6c65886d2dd27b66d92632232adc64f92145bf8403fe64d5ffa5caea",
    "zh:f79d6155cda7d559c60d74883a24879a01c4d5f6fd7e8d1e3250f3cd215fb904",
    "zh:fd59fa73074805c3575f08cd627eef7acda14ab6dac2c135a66e7a38d262201c",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version = "4.0.4"
  hashes = [
    "h1:GZcFizg5ZT2VrpwvxGBHQ/hO9r6g0vYdQqx3bFD3anY=",
    "h1:pe9vq86dZZKCm+8k1RhzARwENslF3SXb9ErHbQfgjXU=",
    "zh:23671ed83e1fcf79745534841e10291bbf34046b27d6e68a5d0aab77206f4a55",
    "zh:45292421211ffd9e8e3eb3655677700e3c5047f71d8f7650d2ce30242335f848",
    "zh:59fedb519f4433c0fdb1d58b27c210b27415fddd0cd73c5312530b4309c088be",
    "zh:5a8eec2409a9ff7cd0758a9d818c74bcba92a240e6c5e54b99df68fff312bbd5",
    "zh:5e6a4b39f3171f53292ab88058a59e64825f2b842760a4869e64dc1dc093d1fe",
    "zh:810547d0bf9311d21c81cc306126d3547e7bd3f194fc295836acf164b9f8424e",
    "zh:824a5f3617624243bed0259d7dd37d76017097dc3193dac669be342b90b2ab48",
    "zh:9361ccc7048be5dcbc2fafe2d8216939765b3160bd52734f7a9fd917a39ecbd8",
    "zh:aa02ea625aaf672e649296bce7580f62d724268189fe9ad7c1b36bb0fa12fa60",
    "zh:c71b4cd40d6ec7815dfeefd57d88bc592c0c42f5e5858dcc88245d371b4b8b1e",
    "zh:dabcd52f36b43d250a3d71ad7abfa07b5622c69068d989e60b79b2bb4f220316",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/olivr/gpg" {
  version = "0.2.1"
  hashes = [
    "h1:+IxSHXkyhHgszKVym4qfp9rF8U0a1a2But15WdA1Xlg=",
    "h1:1aDT6MVlRnC8qF68rHe1NhCdzz/+tWfix7Aftl3s/qw=",
    "zh:014bb7c1140dd2bb96b13a729d450e0aaa743b7f233884ab471a1e029853e66b",
    "zh:262fafa02f79402373b34c7a3bcb1b7b611a6e1fb6713b38480359d8582670e8",
    "zh:5202f998398f8b0d0a9cce7c1dd891ecce493b7aeb23d970ba5319cfbb024086",
    "zh:63609d1e453442453834bf7196e79d29d9340c76f172aec21c321aa13880011a",
    "zh:7fda5a60fe84c4e592087befdbba0b28f0b7f2218d81474720a79bfc587745e8",
    "zh:7fe14800b15f3a79efe9222047c34d0347c0a904356205c9a38abb5a668bc1cc",
    "zh:85f80b1a074b1e616c15c78d6d97c802ee0b5bd2bd878febc8f8430467de9819",
    "zh:a38ba53bd08827ef648f082efb216f65174ab2b18b5d6e14daf9b15ff8a9dfe8",
    "zh:a7c5c35cac6466d60feabb5e2e27a8d582f104152b7e5b87adb0071108b413a6",
    "zh:b6983ea1502698a74215fe2a52b3b81b48dc539daf5d9abe681ef9bcd9b67453",
    "zh:da2a64d029a496d20cfd23bc63a6943e8b80ceead96f5967a2e040eb5e1aeac4",
    "zh:e12b1fb462ab0df7cea32da5f3263d7c10305419dc86118e41687bb863737324",
    "zh:e15335ccd1589cccfa539f0b17b7736b57175ce0ac2e0aad87800b492ee066bf",
    "zh:ebc1e1032e6c996cb3295977196ee87f8babbf38c427bcbd2d8eca41d65f9ffd",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.18.0"
  constraints = "~> 2.18.0"
  hashes = [
    "h1:CDY94t+q+2brGFGDvqx/jTb/322aTvKLgRgjwCaXfI8=",
    "h1:G0cCjSOF3HuxZn25ph/+FdD/xFIZOqNrbb8ls9FdWeQ=",
    "zh:280380b562e209fca056c8ea28fa13a7f5e6d4c84f35be5f8a7c7c6b21d9e1f2",
    "zh:3a84a8c2f04692beba417998959fcfa8e5b5e1d414aac73ac827718cc8a9d27b",
    "zh:49fc5e20d18f0b3745bcd26398f29482f83db182a30a4b717acf0d7d7352bb26",
    "zh:5602e6adb8289a5027847f31b25d27e77fc02ae5e599b40a0541096d4f618d02",
    "zh:67cc693e61241fd10bcd1bd71a624b4aff03b305e149e2589478a483122b7d9c",
    "zh:83bbae15b6e837072951dc6c4f32bd71fec0cd2f83e6710c1e364bbd67be1dc1",
    "zh:a44bf9d4cd9123ba83d89d632e610a249e5864e18cea4594299fac8eda582eaa",
    "zh:be1624e19eac01fb63420b1e8d441dfcb49a62a3a24403aa15a00413011d24eb",
    "zh:c7cbdc1e3cbdf28d5ccb64a59ea06d96d2f4991f2f62357230065fb54a0421f4",
    "zh:d83622b999265c966bc6856848aafed72624f883cbb19a1a51ac3c0ea014fe06",
    "zh:d869b4509806f0af064eebeb8775ab0cea55dd115aea72a841fecfe7b45a0c6e",
    "zh:dd72830cfdb007b5468689bd9442b6a94728d061b7463676878f5e2669b18ccb",
    "zh:f1345d6d5c942286f4894bfecc34ca21f582f6b09c5cac9c70814f8cddae813d",
    "zh:fcacf999d964c7b28e3e52c2d4efa4490bb332c69fbd6a37d426b629c59f440f",
  ]
}
