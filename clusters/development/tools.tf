module "zammad" {
  source              = "./tools/zammad"
  dns_zone_incubateur = var.dns_zone_incubateur
}

module "matomo" {
  source   = "../../tools/matomo"
  hostname = "matomo${trimprefix(scaleway_domain_record.dev_wildcard.name, "*")}.${scaleway_domain_record.dev_wildcard.dns_zone}"
  providers = {
    scaleway.default-project = scaleway.default_project
    scaleway                 = scaleway
  }
}

module "grist" {
  source              = "./tools/grist"
  scaleway_access_key = var.scaleway_access_key
  scaleway_secret_key = var.scaleway_secret_key
  scaleway_project_id = var.scaleway_project_id
  dns_zone_incubateur = var.dns_zone_incubateur
  oauth_domain        = var.grist_oauth_domain
  oauth_client_id     = var.grist_oauth_client_id
  oauth_client_secret = var.grist_oauth_client_secret
  default_email       = var.grist_default_email

  scaleway_default_access_key = var.scaleway_default_access_key
  scaleway_default_secret_key = var.scaleway_default_secret_key
  scaleway_default_project_id = var.scaleway_default_project_id

  monitoring_org_id = var.grist_monitoring_org_id
}
