locals {
  grafana_host = "${scaleway_domain_record.grafana.name}.${var.dns_zone_incubateur}"
}

resource "scaleway_domain_record" "grafana" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "grafana.dev"
  type     = "CNAME"
  data     = local.cluster_cname
  ttl      = 3600
}

resource "scaleway_object_bucket" "logs" {
  name = "anct-incubateur-logs-dev"
}
resource "scaleway_object_bucket" "mimir_alertmanager_storage" {
  name = "anct-incubateur-mimir-alertmanager-storage-dev"
}
resource "scaleway_object_bucket" "mimir_blocks_storage" {
  name = "anct-incubateur-mimir-blocks-storage-dev"
}
resource "scaleway_object_bucket" "mimir_ruler_storage" {
  name = "anct-incubateur-mimir-ruler-storage-dev"
}

module "monitoring" {
  source   = "../../modules/monitoring"
  hostname = local.grafana_host

  loki_logs_bucket          = scaleway_object_bucket.logs.name
  mimir_alertmanager_bucket = scaleway_object_bucket.mimir_alertmanager_storage.name
  mimir_blocks_bucket       = scaleway_object_bucket.mimir_blocks_storage.name
  mimir_ruler_bucket        = scaleway_object_bucket.mimir_ruler_storage.name
  bucket_region             = "fr-par"
  bucket_endpoint           = "https://s3.fr-par.scw.cloud"
  bucket_access_key         = var.scaleway_access_key
  bucket_secret_key         = var.scaleway_secret_key

  smtp_host     = var.sendinblue_smtp_host
  smtp_port     = var.sendinblue_smtp_port
  smtp_user     = var.sendinblue_smtp_user
  smtp_password = var.sendinblue_smtp_password
  smtp_from     = var.sendinblue_smtp_user
}

moved {
  from = kubernetes_namespace.monitoring
  to   = module.monitoring.kubernetes_namespace.monitoring
}
moved {
  from = random_password.main_org_id
  to   = module.monitoring.random_password.main_org_id
}

moved {
  from = module.grafana
  to   = module.monitoring.module.grafana
}
moved {
  from = module.loki
  to   = module.monitoring.module.loki
}
moved {
  from = module.cortex_tenant
  to   = module.monitoring.module.cortex_tenant
}
moved {
  from = module.mimir
  to   = module.monitoring.module.mimir
}
moved {
  from = module.promtail
  to   = module.monitoring.module.promtail
}
moved {
  from = module.prometheus
  to   = module.monitoring.module.prometheus
}

moved {
  from = grafana_data_source.mimir
  to   = module.monitoring.grafana_data_source.mimir
}
moved {
  from = grafana_data_source.mimir_old
  to   = module.monitoring.grafana_data_source.mimir_old
}
moved {
  from = grafana_data_source.loki
  to   = module.monitoring.grafana_data_source.loki
}
