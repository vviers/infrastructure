terraform {
  required_providers {
    kubernetes = {
      version = "~> 2.13.1"
      source  = "hashicorp/kubernetes"
    }
    grafana = {
      source = "grafana/grafana"
    }
    helm = {
      version = "~> 2.5.0"
      source  = "hashicorp/helm"
    }
    scaleway = {
      source = "scaleway/scaleway"
      configuration_aliases = [
        scaleway,
        scaleway.iam,
        scaleway.default_project
      ]
    }
  }
}
