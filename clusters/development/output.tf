output "scaleway_cluster_id" {
  value = scaleway_k8s_cluster.development.id
}

output "haproxy_ip" {
  value = scaleway_lb_ip.haproxy_ip.ip_address
}
output "cluster_cname" {
  value = local.cluster_cname
}

output "grafana_auth" {
  value     = module.monitoring.grafana_auth
  sensitive = true
}

output "grafana_url" {
  value = module.monitoring.grafana_url
}

output "loki_url" {
  value = module.monitoring.loki_service_url
}

output "prometheus_url" {
  value = module.monitoring.mimir_internal_prometheus_url
}
