resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  depends_on = [scaleway_k8s_pool.default]
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.6.1"
  wait       = true
  namespace  = kubernetes_namespace.cert_manager.metadata[0].name
  set {
    name  = "installCRDs"
    value = true
  }
}

resource "helm_release" "cert_manager_scaleway_webhook" {
  name       = "cert-manager-scaleway-webhook"
  repository = "https://gitlab.com/api/v4/projects/41792297/packages/helm/stable"
  chart      = "scaleway-webhook"
  version    = "0.0.1"
  namespace  = helm_release.cert_manager.namespace
  set_sensitive {
    name  = "secret.accessKey"
    value = var.scaleway_default_access_key
  }
  set_sensitive {
    name  = "secret.secretKey"
    value = var.scaleway_default_secret_key
  }
}

resource "kubernetes_manifest" "cert_manager_clusterissuer_letsencrypt_staging" {
  depends_on = [helm_release.cert_manager]

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-staging"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-staging"
        }
        "server" = "https://acme-staging-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "class" = "haproxy"
              }
            }
            "selector" = {}
          },
          {
            "dns01" = {
              "webhook" = {
                "groupName"  = "acme.scaleway.com"
                "solverName" = "scaleway"
              }
            }
          }
        ]
      }
    }
  }
}

resource "kubernetes_manifest" "cert_manager_clusterissuer_letsencrypt_prod" {
  depends_on = [helm_release.cert_manager]

  manifest = {
    "apiVersion" = "cert-manager.io/v1"
    "kind"       = "ClusterIssuer"
    "metadata" = {
      "name" = "letsencrypt-prod"
    }
    "spec" = {
      "acme" = {
        "privateKeySecretRef" = {
          "name" = "letsencrypt-prod"
        }
        "server" = "https://acme-v02.api.letsencrypt.org/directory"
        "solvers" = [
          {
            "http01" = {
              "ingress" = {
                "class" = "haproxy"
              }
            }
            "selector" = {}
          },
          {
            "dns01" = {
              "webhook" = {
                "groupName"  = "acme.scaleway.com"
                "solverName" = "scaleway"
              }
            }
          }
        ]
      }
    }
  }
}

moved {
  from = kubernetes_namespace.cert-manager
  to   = kubernetes_namespace.cert_manager
}
moved {
  from = helm_release.cert-manager
  to   = helm_release.cert_manager
}
moved {
  from = kubernetes_manifest.cert-manager-clusterissuer-letsencrypt-staging
  to   = kubernetes_manifest.cert_manager_clusterissuer_letsencrypt_staging
}
moved {
  from = kubernetes_manifest.cert-manager-clusterissuer-letsencrypt-prod
  to   = kubernetes_manifest.cert_manager_clusterissuer_letsencrypt_prod
}
