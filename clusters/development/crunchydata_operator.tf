resource "kubernetes_namespace" "crunchydata_pgo" {
  metadata {
    name = "postgresql-operator"
  }
}

module "crunchydata_pgo" {
  source        = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgo"
  version       = "0.0.3"
  namespace     = kubernetes_namespace.crunchydata_pgo.metadata[0].name
  chart_name    = "pgo"
  chart_version = "5.3.1"
}

moved {
  from = kubernetes_namespace.crunchydata-pgo
  to   = kubernetes_namespace.crunchydata_pgo
}
moved {
  from = module.crunchydata-pgo
  to   = module.crunchydata_pgo
}
