resource "kubernetes_namespace" "redis" {
  metadata {
    name = "redis-operator"
  }
}

module "redis" {
  source  = "gitlab.com/vigigloo/tools-k8s-redis/operator"
  version = "0.0.1"

  chart_name    = "redis"
  chart_version = "0.14.3"
  namespace     = kubernetes_namespace.redis.metadata[0].name
}
