resource "kubernetes_namespace" "kyverno" {
  metadata {
    name = "kyverno-operator"
  }
}

module "kyverno" {
  source  = "gitlab.com/vigigloo/tools-k8s/kyverno"
  version = "0.0.2"

  chart_name    = "kyverno"
  chart_version = "2.7.2"
  namespace     = kubernetes_namespace.kyverno.metadata[0].name
}
