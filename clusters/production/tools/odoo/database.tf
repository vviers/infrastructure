locals {
  s3_backup_region       = "fr-par"
  s3_backups_bucket_name = "incubateur-odoo-backups"
}
resource "scaleway_object_bucket" "odoo_backups" {
  name   = local.s3_backups_bucket_name
  region = local.s3_backup_region
}
