resource "scaleway_domain_record" "odoo" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "odoo"
  type     = "ALIAS"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.odoo.name}.${scaleway_domain_record.odoo.dns_zone}"
}

moved {
  from = scaleway_domain_record.chat
  to   = scaleway_domain_record.odoo
}
