module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = "6"
  max_memory_limits = "8Gi"
  namespace         = "odoo"
  project_name      = "Odoo"
  project_slug      = "odoo"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

resource "random_password" "postgresql_password" {
  length = 96
}
locals {
  chart_name = "odoo"
}
module "odoo" {
  source  = "gitlab.com/vigigloo/tools-k8s/odoo"
  version = "0.0.4"

  chart_name    = local.chart_name
  chart_version = "23.2.4"
  namespace     = module.namespace.namespace

  backup            = true
  backup_bucket     = scaleway_object_bucket.odoo_backups.name
  backup_region     = scaleway_object_bucket.odoo_backups.region
  backup_access_key = var.scaleway_access_key
  backup_secret_key = var.scaleway_secret_key
  backup_user       = "postgres"
  backup_password   = random_password.postgresql_password.result

  values = [
    <<-EOT
    smtpHost: smtp.tem.scw.cloud
    smtpPort: 465
    smtpProtocol: tls
    smtpUser: 0e5249bc-c22c-42a9-b30d-6105fc814d75
    smtpPassword: ${scaleway_iam_api_key.odoo.secret_key}
    postgresql:
      global:
        postgresql:
          auth:
            postgresPassword: ${random_password.postgresql_password.result}
      primary:
        resources:
          limits:
            memory: 1024Mi
            cpu: 400m
          requests:
            memory: 512Mi
            cpu: 400m
    service:
      type: ClusterIP
    ingress:
      enabled: true
      ingressClassName: null
      hostname: ${local.hostname}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
        haproxy.org/response-set-header: Content-Security-Policy upgrade-insecure-requests
      tls: true
    resources:
      limits:
        memory: 512Mi
      requests:
        cpu: 150m
    affinity:
      podAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchExpressions:
            - key: app.kubernetes.io/name
              operator: In
              values:
              - odoo
          topologyKey: kubernetes.io/hostname
    EOT
  ]
}
