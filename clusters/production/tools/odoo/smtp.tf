resource "scaleway_tem_domain" "odoo" {
  provider   = scaleway.default_project
  accept_tos = true
  name       = local.hostname
}

resource "scaleway_domain_record" "spf" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "odoo"
  type     = "TXT"
  data     = "v=spf1 ${scaleway_tem_domain.odoo.spf_config} -all"
}

resource "scaleway_domain_record" "dkim_mail" {
  provider = scaleway.default_project
  dns_zone = var.dns_zone_incubateur
  name     = "0e5249bc-c22c-42a9-b30d-6105fc814d75._domainkey.odoo"
  type     = "TXT"
  data     = scaleway_tem_domain.odoo.dkim_config
}

resource "scaleway_iam_application" "odoo" {
  provider = scaleway.iam
  name     = "Odoo(${local.hostname})"
}

resource "scaleway_iam_api_key" "odoo" {
  provider       = scaleway.iam
  application_id = scaleway_iam_application.odoo.id
  description    = "[Terraform] API key to send emails via transactional API"
}

resource "scaleway_iam_policy" "odoo_transac_email" {
  provider       = scaleway.iam
  name           = "Email sending policy for ${local.hostname}"
  description    = "[Terraform] Allow to send email"
  application_id = scaleway_iam_application.odoo.id
  rule {
    project_ids          = [var.scaleway_default_project_id]
    permission_set_names = ["TransactionalEmailEmailFullAccess"]
  }
}
