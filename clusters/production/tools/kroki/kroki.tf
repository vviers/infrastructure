module "kroki" {
  source        = "gitlab.com/vigigloo/tools-k8s/kroki"
  version       = "0.0.1"
  chart_name    = "kroki"
  chart_version = "0.1.3"
  depends_on    = [scaleway_domain_record.kroki]
  namespace     = module.namespace.namespace
  values = [
    <<-EOT
    companions:
      blockdiag:
        enabled: true
        resources:
          limits:
            memory: 256Mi
          requests:
            cpu: 50m
      mermaid:
        enabled: true
        resources:
          limits:
            memory: 256Mi
          requests:
            cpu: 50m
      bpmn:
        enabled: true
        resources:
          limits:
            memory: 256Mi
          requests:
            cpu: 50m
      excalidraw:
        enabled: true
        resources:
          limits:
            memory: 256Mi
          requests:
            cpu: 50m
    ingress:
      enabled: true
      className: null
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      hosts:
        - host: ${local.hostname}
          paths:
            - path: "/"
              pathType: "Prefix"
      tls:
        - hosts:
            - ${local.hostname}
          secretName: kroki-tls
    resources:
      limits:
        memory: 256Mi
      requests:
        cpu: 50m
    EOT
  ]

}
