module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.2"
  max_cpu_requests  = "3"
  max_memory_limits = "8Gi"
  namespace         = "kroki"
  project_name      = "Kroki"
  project_slug      = "kroki"
}
