resource "scaleway_domain_record" "kroki" {
  dns_zone = var.dns_zone_incubateur
  name     = "kroki"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.kroki.name}.${scaleway_domain_record.kroki.dns_zone}"
}
