resource "scaleway_domain_record" "directus" {
  provider = scaleway.default-project
  dns_zone = var.dns_zone_incubateur
  name     = "directus"
  type     = "CNAME"
  data     = var.cluster_cname
  ttl      = 3600
}

locals {
  hostname = "${scaleway_domain_record.directus.name}.${scaleway_domain_record.directus.dns_zone}"
}
