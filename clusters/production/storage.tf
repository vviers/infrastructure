resource "kubernetes_annotations" "default_storage_class_is_not_delete" {
  api_version = "storage.k8s.io/v1"
  kind        = "StorageClass"
  metadata {
    name = "scw-bssd"
  }
  annotations = {
    "storageclass.kubernetes.io/is-default-class" = false
  }
  force = true
}

resource "kubernetes_annotations" "default_storage_class_is_retain" {
  api_version = "storage.k8s.io/v1"
  kind        = "StorageClass"
  metadata {
    name = "scw-bssd-retain"
  }
  annotations = {
    "storageclass.kubernetes.io/is-default-class" = true
  }
  force = true
}
