provider "kubernetes" {
  host                   = scaleway_k8s_cluster.production.kubeconfig[0].host
  token                  = scaleway_k8s_cluster.production.kubeconfig[0].token
  cluster_ca_certificate = base64decode(scaleway_k8s_cluster.production.kubeconfig[0].cluster_ca_certificate)
}

provider "helm" {
  kubernetes {
    host                   = scaleway_k8s_cluster.production.kubeconfig[0].host
    token                  = scaleway_k8s_cluster.production.kubeconfig[0].token
    cluster_ca_certificate = base64decode(scaleway_k8s_cluster.production.kubeconfig[0].cluster_ca_certificate)
  }
}
