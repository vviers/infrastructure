module "forum" {
  source              = "./external-projects/forum"
  dns_zone_incubateur = var.dns_zone_incubateur
  scaleway_access_key = var.scaleway_default_access_key
  scaleway_secret_key = var.scaleway_default_secret_key
  scaleway_project_id = var.scaleway_default_project_id
}

module "territoires_store" {
  source              = "./external-projects/territoires.store"
  scaleway_access_key = var.scaleway_default_access_key
  scaleway_secret_key = var.scaleway_default_secret_key
  scaleway_project_id = var.scaleway_default_project_id
  loadbalancer_ip     = scaleway_lb_ip.haproxy_ip.ip_address
}

moved {
  from = module.territoires-store
  to   = module.territoires_store
}
