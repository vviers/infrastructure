resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"
  }
}

resource "random_password" "main_org_id" {
  length  = 63
  special = false
}

module "loki" {
  source     = "gitlab.com/vigigloo/tools-k8s-grafana/loki"
  version    = "0.0.3"
  chart_name = "loki"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  values = [
    <<-EOT
    global:
      dnsService: coredns
    compactor:
      enabled: true
    loki:
      structuredConfig:
        auth_enabled: true
        compactor:
          shared_store: aws
          retention_enabled: true
          retention_delete_delay: 360h
      schemaConfig:
        configs:
          - from: 2022-08-01
            store: boltdb-shipper
            object_store: aws
            schema: v11
            index:
              prefix: loki_index_
              period: 24h
      storageConfig:
        boltdb_shipper:
          shared_store: aws
        aws:
          endpoint: ${var.bucket_endpoint}
          access_key_id: ${var.bucket_access_key}
          secret_access_key: ${var.bucket_secret_key}
          region: ${var.bucket_region}
          bucketnames: ${var.loki_logs_bucket}
    EOT
  ]
}

module "grafana" {
  source                    = "gitlab.com/vigigloo/tools-k8s-grafana/grafana"
  version                   = "0.0.3"
  chart_name                = "grafana"
  namespace                 = kubernetes_namespace.monitoring.metadata[0].name
  grafana_admin_secret_name = "grafana-terraform-user"
  grafana_admin_user        = "terraform"

  values = [
    <<-EOT
    image:
      tag: 9.1.0
    grafana.ini:
      server:
        root_url: https://${var.hostname}/
      smtp:
        enabled: true
        host: ${var.smtp_host}:${var.smtp_port}
        user: ${var.smtp_user}
        password: ${var.smtp_password}
        from_address: ${var.smtp_from}
    persistence:
      enabled: true
    ingress:
      enabled: true
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
      path: /

      hosts:
        - ${var.hostname}

      tls:
        - secretName: grafana-tls
          hosts:
            - ${var.hostname}
    EOT
  ]
}

module "promtail" {
  source     = "gitlab.com/vigigloo/tools-k8s-grafana/promtail"
  version    = "0.0.1"
  chart_name = "promtail"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  depends_on = [module.loki]

  values = [
    <<-EOT
    config:
      snippets:
        extraRelabelConfigs:
          - action: replace
            source_labels:
              - __meta_kubernetes_pod_annotation_monitoring_org_id
            target_label: _monitoring_org_id
        pipelineStages:
          - cri: {}
          - json:
              expressions:
                output: log
                stream: stream
                timestamp: time
          - tenant:
              label: _monitoring_org_id
          - labeldrop:
              - _monitoring_org_id
      clients:
        - url: http://${module.loki.internal_gateway}/loki/api/v1/push
        - url: http://${module.loki.internal_gateway}/loki/api/v1/push
          tenant_id: ${random_password.main_org_id.result}
    EOT
  ]
}

module "mimir" {
  source     = "gitlab.com/vigigloo/tools-k8s-grafana/mimir"
  version    = "0.0.3"
  chart_name = "mimir"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name

  values = [
    <<-EOT
    global:
      dnsService: coredns
    minio:
      enabled: false
    mimir:
      structuredConfig:
        alertmanager_storage:
          backend: s3
          s3:
            access_key_id: ${var.bucket_access_key}
            bucket_name: ${var.mimir_alertmanager_bucket}
            region: ${var.bucket_region}
            endpoint: ${local.bucket_endpoint_without_scheme}
            secret_access_key: ${var.bucket_secret_key}
        blocks_storage:
          backend: s3
          s3:
            access_key_id: ${var.bucket_access_key}
            bucket_name: ${var.mimir_blocks_bucket}
            region: ${var.bucket_region}
            endpoint: ${local.bucket_endpoint_without_scheme}
            secret_access_key: ${var.bucket_secret_key}
        ruler_storage:
          backend: s3
          s3:
            access_key_id: ${var.bucket_access_key}
            bucket_name: ${var.mimir_ruler_bucket}
            region: ${var.bucket_region}
            endpoint: ${local.bucket_endpoint_without_scheme}
            secret_access_key: ${var.bucket_secret_key}
        limits:
          max_global_series_per_user: 1000000
          max_label_names_per_series: 35
    ingester:
      persistentVolume:
        size: 4Gi
    EOT
  ]
}

module "prometheus" {
  source     = "gitlab.com/vigigloo/tools-k8s/prometheus"
  version    = "0.0.1"
  chart_name = "prometheus"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  values = [
    <<-EOT
    server:
      remoteWrite:
        - url: ${module.cortex_tenant.internal_url}/push
      persistentVolume:
        size: 16Gi
    EOT
    , file("${path.module}/prometheus.yaml")
  ]
}

module "cortex_tenant" {
  source                     = "gitlab.com/vigigloo/tools-k8s-cortex/tenant"
  version                    = "0.0.2"
  chart_name                 = "cortex-tenant"
  namespace                  = kubernetes_namespace.monitoring.metadata[0].name
  cortex_tenant_label        = "monitoring_org_id"
  cortex_tenant_label_remove = true
  cortex_tenant_target       = module.mimir.internal_push_url
  cortex_tenant_default      = random_password.main_org_id.result
}

resource "grafana_data_source" "mimir" {
  name = "mimir"
  type = "prometheus"
  url  = "${module.mimir.internal_nginx_url}/prometheus"

  http_headers = {
    X-Scope-OrgID = random_password.main_org_id.result
  }
  is_default = true
}

resource "grafana_data_source" "mimir_old" {
  name = "mimir-old"
  type = "prometheus"
  url  = "${module.mimir.internal_nginx_url}/prometheus"

  http_headers = {
    X-Scope-OrgID = "default"
  }
}

resource "grafana_data_source" "loki" {
  name = "loki"
  type = "loki"
  url  = module.loki.service_url

  http_headers = {
    X-Scope-OrgID = random_password.main_org_id.result
  }
}
