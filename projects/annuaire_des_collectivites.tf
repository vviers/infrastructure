locals {
  collectivite_fr_dns_zone = "collectivite.fr"
}
module "annuaire_des_collectivites" {
  source       = "./generic"
  common       = local.common
  project_name = "AnnuaireDesCollectivites"
  project_slug = "annuaire-des-collectivites"

  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam = scaleway.iam
  }
}

resource "scaleway_domain_record" "annuaire_des_collectivites_record" {
  dns_zone = local.collectivite_fr_dns_zone
  name     = ""
  type     = "ALIAS"
  data     = var.production_cluster_cname
}

resource "scaleway_domain_record" "annuaire_des_collectivites_record_wildcard" {
  dns_zone = local.collectivite_fr_dns_zone
  name     = "*"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 60
}

# An MX record can't point directly to the IP of the mail server, so we use an intermediary record
resource "scaleway_domain_record" "collectivite_fr_mx_mx" {
  dns_zone = local.collectivite_fr_dns_zone
  name     = ""
  type     = "MX"
  data     = "mx.collectivite.fr."
}
resource "scaleway_domain_record" "collectivite_fr_mx" {
  dns_zone = local.collectivite_fr_dns_zone
  name     = "mx"
  type     = "A"
  data     = "51.159.128.187"
}
resource "scaleway_domain_record" "collectivite_fr_spf" {
  dns_zone = local.collectivite_fr_dns_zone
  name     = ""
  type     = "TXT"
  data     = "v=spf1 ip4:51.159.128.187 +a +mx ~all"
}
resource "scaleway_domain_record" "collectivite_fr_dkim" {
  dns_zone = local.collectivite_fr_dns_zone
  name     = "default._domainkey"
  type     = "TXT"
  data     = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApxgJjReltM0RgPv9IvCFhK7B1SGbpQRTh4kRcubOkfIyxR0w9IDyA5AL1LOcEECt55Sov8DQK2dsybQflvmiQlw/7/IZRES51yzBPEDYqcopmBulGXyY70G8X84iC9MR5MjY1rrSRnxnQ3BlnE9KD5QblnNFKKgSsTtaHgGesYmjohk6xBtZ5wICmjpiUad+V32YA1M7cNm3GfcJIQIts0+OrqUmHJvym4iwoSOwKJiwjNSh0A/U4UNNuc8tvDN2vSHf9Vd6NSazWepK4QjOn3ssXKSGkWGCcM4Mr5Svu2KrAPknfMhyQYZvUKJGzHCVymZVfOPnJr4fg56kcpknaQIDAQAB;"
}
