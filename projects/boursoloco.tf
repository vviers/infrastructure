module "boursoloco" {
  source       = "./generic"
  common       = local.common
  project_name = "BoursoLoco"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
