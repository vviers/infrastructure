data "scaleway_instance_server" "donnees_vm" {
  name = "donnees-territoires"
  zone = "fr-par-2"
}
resource "scaleway_domain_record" "sit" {
  dns_zone = var.dns_zone_incubateur
  name     = "sit"
  type     = "A"
  data     = data.scaleway_instance_server.donnees_vm.public_ip
  ttl      = 3600
}

# Le site web statique de Données et Territoires (servi via GitLab Pages)
resource "scaleway_domain_record" "donnees" {
  dns_zone = var.dns_zone_incubateur
  name     = "donnees"
  type     = "A"
  data     = data.scaleway_instance_server.donnees_vm.public_ip
  ttl      = 3600
}

# Le site web statique des fiches territoriales (servi via GitLab Pages)
resource "scaleway_domain_record" "fiches" {
  dns_zone = var.dns_zone_incubateur
  name     = "fiches"
  type     = "A"
  data     = data.scaleway_instance_server.donnees_vm.public_ip
  ttl      = 3600
}

resource "scaleway_domain_record" "donnees_formulaires" {
  dns_zone = var.dns_zone_incubateur
  name     = "formulaires"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}

module "donnees_et_territoires" {
  source            = "./generic"
  common            = local.common
  project_name      = "Donnees Et Territoires"
  project_slug      = "donneesetterritoires"
  project_subdomain = "donnees"
  with_record       = false
  with_scw_project  = true
  providers = {
    scaleway.iam = scaleway.iam
  }
}

resource "scaleway_domain_record" "donnees_gitlab_pages" {
  dns_zone = var.dns_zone_incubateur
  name     = "_gitlab-pages-verification-code.donnees"
  type     = "TXT"
  data     = "gitlab-pages-verification-code=6c11661347a05505ba4b770a95fc2724"
}

resource "scaleway_domain_record" "donnees_sit" {
  dns_zone = var.dns_zone_incubateur
  name     = "*.sit"
  type     = "A"
  data     = data.scaleway_instance_server.donnees_vm.public_ip
  ttl      = 60
}
