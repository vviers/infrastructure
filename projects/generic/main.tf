locals {
  tf_project_switch = var.with_tf_project ? 1 : 0
}
module "grafana_production" {
  source = "../../modules/grafana_api_creation"
  grafana = {
    url     = var.common.grafana_production_url
    api_key = var.common.grafana_production_auth
  }
  project_name = local.project_name
}

module "grafana_development" {
  source = "../../modules/grafana_api_creation"
  grafana = {
    url     = var.common.grafana_development_url
    api_key = var.common.grafana_development_auth
  }
  project_name = local.project_name
}

module "gitlab_project" {
  count   = local.tf_project_switch
  source  = "gitlab.com/vigigloo/tf-modules/gitlabterraformproject"
  version = "0.6.1"

  gitlab_project_location = var.common.projects_group_id
  project_name            = local.project_name
  project_slug            = local.project_slug

  dev_base-domain  = "${local.project_subdomain}.${var.common.development_base_domain}"
  prod_base-domain = "${local.project_subdomain}.${var.common.production_base_domain}"

  no_gitlab_tokens = true

  scaleway_organization_id                = var.common.scaleway_organization_id
  scaleway_cluster_development_cluster_id = var.common.scaleway_cluster_development_cluster_id
  scaleway_cluster_production_cluster_id  = var.common.scaleway_cluster_production_cluster_id

  grafana_development = {
    url            = var.common.grafana_development_url
    api_key        = module.grafana_development.api_key
    org_id         = module.grafana_development.org_id
    loki_url       = var.common.grafana_development_loki_url
    prometheus_url = var.common.grafana_development_prometheus_url
  }
  grafana_production = {
    url            = var.common.grafana_production_url
    api_key        = module.grafana_production.api_key
    org_id         = module.grafana_production.org_id
    loki_url       = var.common.grafana_production_loki_url
    prometheus_url = var.common.grafana_production_prometheus_url
  }
}

output "var_file" {
  value = var.with_tf_project ? module.gitlab_project[0].var_file : ""
}

moved {
  from = module.project
  to   = module.gitlab_project
}
moved {
  from = module.gitlab_project
  to   = module.gitlab_project[0]
}
