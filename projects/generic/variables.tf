variable "project_name" {
  type = string
}

variable "project_slug" {
  type    = string
  default = null
}

variable "project_subdomain" {
  type    = string
  default = null
}

variable "with_tf_project" {
  type    = bool
  default = true
}
variable "with_scw_project" {
  type    = bool
  default = false
}
variable "scw_custom_groups" {
  type        = map(list(string))
  default     = {}
  description = "Custom groups to create with specific project resources access. Specify a map where each key is use for the group's name and policy, and a list of Scaleway IAM permissions."
}
variable "with_record" {
  type    = bool
  default = true
}
variable "with_wildcard_record" {
  type    = bool
  default = true
}

variable "common" {
  type = object({
    projects_group_id                       = number
    dns_zone_incubateur                     = string
    production_cluster_cname                = string
    development_cluster_cname               = string
    development_base_domain                 = string
    production_base_domain                  = string
    scaleway_organization_id                = string
    scaleway_cluster_development_cluster_id = string
    scaleway_cluster_production_cluster_id  = string
    grafana_production_url                  = string
    grafana_production_auth                 = string
    grafana_production_loki_url             = string
    grafana_production_prometheus_url       = string
    grafana_development_url                 = string
    grafana_development_auth                = string
    grafana_development_loki_url            = string
    grafana_development_prometheus_url      = string

  })
}

locals {
  project_name      = var.project_name
  project_slug      = var.project_slug == null ? lower(replace(local.project_name, " ", "-")) : var.project_slug
  project_subdomain = var.project_subdomain == null ? lower(replace(local.project_slug, " ", "-")) : var.project_subdomain
}
