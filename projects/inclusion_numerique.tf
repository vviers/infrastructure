module "inclusion_numerique" {
  source               = "./generic"
  common               = local.common
  project_name         = "Inclusion Numerique"
  with_tf_project      = false
  with_scw_project     = true
  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam = scaleway.iam
  }
}

resource "scaleway_domain_record" "n8n_inclusion_numerique" {
  dns_zone = var.dns_zone_incubateur
  name     = "n8n.inclusion-numerique"
  type     = "CNAME"
  data     = "inclusion-numerique-n8n.osc-fr1.scalingo.io."
  ttl      = 3600
}
resource "scaleway_domain_record" "dashlord_inclusion_numerique" {
  dns_zone = var.dns_zone_incubateur
  name     = "dashlord.inclusion-numerique"
  type     = "CNAME"
  data     = "inclusion-numerique.github.io."
  ttl      = 3600
}
resource "scaleway_domain_record" "inclusion_numerique_mec_ns0" {
  dns_zone = var.dns_zone_incubateur
  name     = "monespacecollectivite"
  type     = "NS"
  data     = "ns0.dom.scw.cloud"
  ttl      = 3600
}
resource "scaleway_domain_record" "inclusion_numerique_mec_ns1" {
  dns_zone = var.dns_zone_incubateur
  name     = "monespacecollectivite"
  type     = "NS"
  data     = "ns1.dom.scw.cloud"
  ttl      = 3600
}
resource "scaleway_domain_record" "inclusion_numerique_crm_ns0" {
  dns_zone = var.dns_zone_incubateur
  name     = "crm"
  type     = "NS"
  data     = "ns0.dom.scw.cloud"
  ttl      = 3600
}
resource "scaleway_domain_record" "inclusion_numerique_crm_ns1" {
  dns_zone = var.dns_zone_incubateur
  name     = "crm"
  type     = "NS"
  data     = "ns0.dom.scw.cloud"
  ttl      = 3600
}

resource "scaleway_domain_record" "inclusion_numerique_labase_ns0" {
  dns_zone = var.dns_zone_incubateur
  name     = "v2.labase"
  type     = "NS"
  data     = "ns0.dom.scw.cloud"
  ttl      = 3600
}
resource "scaleway_domain_record" "inclusion_numerique_labase_ns1" {
  dns_zone = var.dns_zone_incubateur
  name     = "v2.labase"
  type     = "NS"
  data     = "ns1.dom.scw.cloud"
  ttl      = 3600
}

moved {
  from = scaleway_domain_record.inclusion_numerique_labase_ns0_
  to   = scaleway_domain_record.inclusion_numerique_labase_ns0
}
