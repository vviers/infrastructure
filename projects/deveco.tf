module "deveco" {
  source               = "./generic"
  common               = local.common
  project_name         = "Deveco"
  with_record          = false
  with_wildcard_record = false
  providers = {
    scaleway.iam = scaleway.iam
  }
}

resource "scaleway_domain_record" "deveco" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "A"
  data     = var.production_haproxy_ip
  ttl      = 3600
}

resource "scaleway_domain_record" "deveco_wildcard" {
  dns_zone = var.dns_zone_incubateur
  name     = "*.deveco"
  type     = "CNAME"
  data     = var.production_cluster_cname
  ttl      = 3600
}

resource "scaleway_domain_record" "deveco_google_search_console" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "TXT"
  data     = "google-site-verification=L2JQ-fN4ulue8tUkvSsJqXUR6JoL0KvlhieQMp0olCA"
  ttl      = 3600
}

resource "scaleway_domain_record" "deveco_sib_code" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "TXT"
  data     = "Sendinblue-code:8ff5d53433fed3070b3f1359d24abb8b"
}

resource "scaleway_domain_record" "deveco_ovh_autodiscover" {
  dns_zone = var.dns_zone_incubateur
  name     = "_autodiscover._tcp.deveco"
  type     = "SRV"
  data     = "0 0 443 pro1.mail.ovh.net."
}

resource "scaleway_domain_record" "deveco_ovh_mx0" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx0.mail.ovh.net."
  priority = 1
}
resource "scaleway_domain_record" "deveco_ovh_mx1" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx1.mail.ovh.net."
  priority = 5
}
resource "scaleway_domain_record" "deveco_ovh_mx2" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx2.mail.ovh.net."
  priority = 50
}
resource "scaleway_domain_record" "deveco_ovh_mx3" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "MX"
  data     = "mx3.mail.ovh.net."
  priority = 100
}

resource "scaleway_domain_record" "deveco_spf" {
  dns_zone = var.dns_zone_incubateur
  name     = "deveco"
  type     = "TXT"
  data     = "v=spf1 include:spf.sendinblue.com include:spf.tipimail.com mx ~all"
}
resource "scaleway_domain_record" "deveco_dmarc" {
  dns_zone = var.dns_zone_incubateur
  name     = "_dmarc.deveco"
  type     = "TXT"
  data     = "v=DMARC1; p=none; sp=none; rua=mailto:dmarc@mailinblue.com!10m; ruf=mailto:dmarc@mailinblue.com!10m; rf=afrf; pct=100; ri=86400"
}

resource "scaleway_domain_record" "deveco_dkim_mail" {
  dns_zone = var.dns_zone_incubateur
  name     = "mail._domainkey.deveco"
  type     = "TXT"
  data     = "k=rsa;p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDeMVIzrCa3T14JsNY0IRv5/2V1/v2itlviLQBwXsa7shBD6TrBkswsFUToPyMRWC9tbR/5ey0nRBH0ZVxp+lsmTxid2Y2z+FApQ6ra2VsXfbJP3HE6wAO0YTVEJt1TmeczhEd2Jiz/fcabIISgXEdSpTYJhb0ct0VJRxcg4c8c7wIDAQAB"
}
resource "scaleway_domain_record" "deveco_dkim_mta" {
  dns_zone = var.dns_zone_incubateur
  name     = "mta._domainkey.deveco"
  type     = "TXT"
  data     = "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDh50JnspQbjA8gu9Q7togGADxpXmxSy3Q8MoMBkT16RGuMgad7eNmgS3lCrMB6ScN3EKmFbplk6aBOBJzA0NtQsTRZzBIrK61sBiAgzOn1w2sUC/GS26MslZhOiXNQFqPLDq3RUwediyyR7j29MCJXZ0rfjBDbnNgDmnmyf8LtkwIDAQAB;"
}
