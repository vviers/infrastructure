resource "scaleway_domain_record" "infrastructures_numeriques_mongodb_development" {
  dns_zone = var.dns_zone_incubateur
  name     = "mongodb.infrastructures-numeriques.dev"
  type     = "A"
  data     = "51.159.26.50"
  ttl      = 3600
}

resource "scaleway_domain_record" "infrastructures_numeriques_mongodb_production" {
  dns_zone = var.dns_zone_incubateur
  name     = "mongodb.infrastructures-numeriques"
  type     = "A"
  data     = "51.159.206.74"
  ttl      = 3600
}

resource "scaleway_domain_record" "infrastructures_numeriques" {
  dns_zone = var.dns_zone_incubateur
  name     = "infrastructures-numeriques"
  type     = "A"
  data     = var.production_haproxy_ip
  ttl      = 3600
}

resource "scaleway_domain_record" "infrastructures_numeriques_listmonk" {
  dns_zone = var.dns_zone_incubateur
  name     = "listmonk.infrastructures-numeriques"
  type     = "A"
  data     = var.production_haproxy_ip
  ttl      = 3600
}

module "infrastructures_numeriques" {
  source           = "./generic"
  common           = local.common
  project_name     = "Infrastructures numeriques"
  with_record      = false
  with_scw_project = true
  scw_custom_groups = {
    "read backups" = ["ObjectStorageReadOnly"],
    "listmonk"     = ["TransactionalEmailEmailFullAccess", "ObjectStorageObjectsWrite", "ObjectStorageObjectsRead", "ObjectStorageObjectsDelete"]
  }
  providers = {
    scaleway.iam = scaleway.iam
  }
}

resource "scaleway_iam_application" "infrastructures_numeriques" {
  provider    = scaleway.iam
  name        = "Infrastructures numeriques listmonk"
  description = "Managed by terraform"
}

resource "scaleway_iam_api_key" "infrastructures_numeriques" {
  provider           = scaleway.iam
  application_id     = scaleway_iam_application.infrastructures_numeriques.id
  default_project_id = module.infrastructures_numeriques.scaleway_project
  description        = "Managed by terraform"
}

output "infrastructures_numeriques_api" {
  value = scaleway_iam_api_key.infrastructures_numeriques.secret_key
}

