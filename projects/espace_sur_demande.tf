module "espace_sur_demande" {
  source           = "./generic"
  common           = local.common
  project_name     = "Espace Sur Demande"
  project_slug     = "espace-demande"
  with_scw_project = true
  providers = {
    scaleway.iam = scaleway.iam
  }
}
