module "licence_libre" {
  source       = "./generic"
  common       = local.common
  project_name = "Licence libre"
  providers = {
    scaleway.iam = scaleway.iam
  }
}
