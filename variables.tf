variable "scaleway_default_project_id" {
  type = string
}

variable "scaleway_iam_access_key" {
  type      = string
  sensitive = true
}
variable "scaleway_iam_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_default_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_organization_id" {
  type = string
}

variable "scaleway_default_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_cluster_development_project_id" {
  type = string
}

variable "scaleway_cluster_development_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_cluster_development_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_cluster_production_project_id" {
  type = string
}

variable "scaleway_cluster_production_access_key" {
  type      = string
  sensitive = true
}

variable "scaleway_cluster_production_secret_key" {
  type      = string
  sensitive = true
}

variable "production_base_domain" {
  type = string
}

variable "development_base_domain" {
  type = string
}

variable "production_tools_mattermost_postgresql" {
  type = string
}

variable "production_tools_outline_postgresql" {
  type = string
}
variable "production_tools_outline_client_id" {
  type = string
}
variable "production_tools_outline_client_secret" {
  type      = string
  sensitive = true
}

variable "production_tools_outline_email_smtp_user" {
  type = string
}
variable "production_tools_outline_email_smtp_password" {
  type      = string
  sensitive = true
}

variable "production_tools_decidim_inclusion_numerique_smtp_user" {
  type = string
}
variable "production_tools_decidim_inclusion_numerique_smtp_password" {
  type      = string
  sensitive = true
}

variable "production_tools_grist_oauth_domain" {
  type = string
}
variable "production_tools_grist_oauth_client_id" {
  type = string
}
variable "production_tools_grist_oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "production_tools_grist_default_email" {
  type = string
}

variable "development_tools_grist_oauth_domain" {
  type = string
}
variable "development_tools_grist_oauth_client_id" {
  type = string
}
variable "development_tools_grist_oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "development_tools_grist_default_email" {
  type = string
}

variable "production_tools_passbolt_smtp_password" {
  type      = string
  sensitive = true
}

variable "sendinblue_smtp_host" {
  type = string
}
variable "sendinblue_smtp_user" {
  type = string
}
variable "sendinblue_smtp_password" {
  type      = string
  sensitive = true
}
variable "sendinblue_smtp_port" {
  type = string
}
variable "sendinblue_smtp_secure" {
  type = string
}
